

import { SkButtonImpl }  from '../../sk-button/src/impl/sk-button-impl.js';


export class DefaultSkButton extends SkButtonImpl {

    get prefix() {
        return 'default';
    }

    get suffix() {
        return 'button';
    }

    bindEvents() {
        super.bindEvents();
    }

    afterRendered() {
        super.afterRendered();
        if (this.buttonType) {
            this.button.classList.add(`sk-btn-${this.buttonType}`);
        }
        this.mountStyles();
    }

}
