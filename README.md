# Skinny Widgets Button for Default Theme


button element

```
npm i sk-button sk-button-jquery --save
```

then add the following to your html

```html
<sk-config
    theme="jquery"
    base-path="/node_modules/sk-core/src"
    theme-path="/node_modules/sk-theme-jquery"
></sk-config>
<sk-button id="myButton" button-type="primary">Ok</sk-button>
<script type="module">
    import { SkButton } from './node_modules/sk-button/index.js';

    customElements.define('sk-button', SkButton);

    myButton.addEventListener('click', (event) => {
        alert('Clicked !');
    });
</script>
```
#### slots

**default (not specified)** - contents inside button

**label** - the same as label

#### template

id: SkButtonTpl